##################################################################################
#
# Author: Jose R. Ortiz-Ubarry
# Email:  jose.ortiz@hpcf.upr.edu
# 
# Description: This program counts the number of flows
#              inside a netflow file given as input using mapreduce.
# 
# Disclaimer: This program was made only for educational purposes under the
#             support of the CPATH project of the University of Puerto Rico.
#             We do not offer any warranties of the software working or used for
#             any other purpose.
#
#            If you use or modify this software please be kind and acknowledge
#            us.
###################################################################################

from disco.core import Job, result_iterator

def map(line, params):
	yield "flow", 1

def reduce(iter, params):
    # Note here I don't need to sort key groups because
    # the only key is flow
    from disco.util import kvgroup
    for word, counts in kvgroup(iter):
      	yield  word, sum(counts)

if __name__ == '__main__':
    job = Job().run(input=["file:///bccd/home/jortiz/netflow-print.txt"],
                    map=map,
                    reduce=reduce)

    # Using a for just for debugging purpose, since it should be
    # only one word flows.
    for word, count in result_iterator(job.wait(show=True)):
        print(count)
