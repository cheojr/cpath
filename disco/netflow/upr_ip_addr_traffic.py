##################################################################################
#
# Author: Jose R. Ortiz-Ubarry
# Email:  jose.ortiz@hpcf.upr.edu
# 
# Description: This program aggregates the traffic for each UPR IP in the connections
#              inside a netflow file given as input using mapreduce.
# 
# Disclaimer: This program was made only for educational purposes under the
#             support of the CPATH project of the University of Puerto Rico.
#             We do not offer any warranties of the software working or used for
#             any other purpose.
#
#            If you use or modify this software please be kind and acknowledge
#            us.
###################################################################################

from disco.core import Job, result_iterator

def map(line, params):
        # Split the flow data into an array
        # Fields 0 and 1 contains the src and dst addresses
        # Field 5 contains the actual traffic in that flow connection.

	data = line.split()
	if data[0][0:7] == params:
		yield data[0], int(data[5])
	if data[1][0:7] == params:
		yield data[1], int(data[5])
	
def reduce(iter, params):
    from disco.util import kvgroup
    for ip, traffic in kvgroup(sorted(iter)):
      	yield  ip, sum(traffic)

if __name__ == '__main__':
    job = Job().run(input=["file:///bccd/home/jortiz/netflow-print-short.txt"],
                    map=map,
                    reduce=reduce, 
		    params="136.145.")
    for ip, traffic in result_iterator(job.wait(show=True)):
        print(ip, traffic)
