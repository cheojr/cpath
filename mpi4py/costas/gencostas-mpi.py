##################################################################################
#
# Author: Jose R. Ortiz-Ubarry
# Email:  jose.ortiz@hpcf.upr.edu
# 
# Description: This is a simple implementation to generate Costas array for any
#              size n using MPI.
# 
# Disclaimer: This program was made only for educational purposes under the
#             support of the CPATH project of the University of Puerto Rico.
#             We do not offer any warranties of the software working or used for
#             any other purpose.
#
#            If you use or modify this software please be kind and acknowledge
#            us.
###################################################################################


import sys
from mpi4py import MPI

comm = MPI.COMM_WORLD   # Defines the default communicator
num_procs = comm.Get_size()  # Stores the number of processes in size.
rank = comm.Get_rank()  # Stores the rank (pid) of the current process
stat = MPI.Status()
tag = None

TAG_WORK=0
TAG_END =1

def findCostas(costas, n, start, current):
        """Completes the searching of Costas arrays that start with the subcostas
           received in the variable costas.
           Pre: Receives a sub array with size less than n, the size of the final array n
                The position where the search space start and the current position to start
                the search.
           Pos: A list with Costas arrays of size n that start with sub array costas."""


	costas_list = []

	while current >= start:
		costas[current] += 1
                while costas[current] in costas[:current] and costas[current] <= n:
               		costas[current]+=1
	
		if costas[current] > n:
                        costas[current] = 0
                        current-=1

		elif isCostas(costas[:current+1], n):
                        if current < n-1:
                                current+=1
                        else:
                                costas_list.append(costas[:])

	return costas_list


def isCostas(array, n):
	"""Checks whether the array received meets the costas property."""

	a_size = len(array)
        for c in range(1, a_size):
        	hash = [0] * (2 * n)
                for i in range(a_size - c):
 	               dif = array[i+c] - array[i]
                       if hash[dif] == 1:
        	               return 0
                       else:
                	       hash[dif] = 1
	return 1


def master(n, sub_n):
	"""Master generates all the sub Costas to send to the slaves, then send them
           on demand to the slaves and keeps them in a global list"""
	
	costas_list = []

	scostas_list = []

	costas = [0] * n
	costas[0] = 1

	start = 0
	current = 1

	# Backtracking non recursive code in python to generate sub Costas. 

	while current >= start:
        	costas[current] += 1
        	while costas[current] in costas[:current] and costas[current] <= n:
                	costas[current]+=1

        	if costas[current] > n:
                	costas[current] = 0
                	current-=1

        	elif isCostas(costas[:current+1], n):
                	if current < sub_n-1:
                        	current+=1
                	else:
                        	scostas_list.append(costas[:])


	# Start sending sub costas to the slaves.
	for i in range(1, num_procs):
		sub_costas = scostas_list.pop(0)
		comm.send(sub_costas, dest=i, tag=TAG_WORK)

	# Collect and send new sub Costas while available.
	while len(scostas_list) > 0:

		sub_costas = scostas_list.pop(0)
		costas_list += comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=stat)
		comm.send(sub_costas, dest=stat.Get_source(), tag=TAG_WORK)

	# Collects the final results and send the EXIT signal.
	for i in range(1, num_procs):
		costas_list += comm.recv(source=i, tag=MPI.ANY_TAG, status=stat)
                comm.send(sub_costas, dest=stat.Get_source(), tag=TAG_END)
		
		
	for costas in costas_list:
		print costas
		

def slave(n, sub_n):
	"""Slaves receives sub Costas from master and completes the search
           of Costas of size n that start with the received sub Costas."""
	
	sub_costas = comm.recv(source=0, tag=MPI.ANY_TAG, status=stat)
	
	while stat.Get_tag() == TAG_WORK:
		costas_list = findCostas(sub_costas, n, sub_n, sub_n)
		comm.send(costas_list, dest=0)
		sub_costas = comm.recv(source=0, tag=MPI.ANY_TAG, status=stat)


def usage():
        print "Usage: python %s <n Costas size> <m sub Costas>" % sys.argv[0]
	print "For m < n"
	print "python %s 8 4" % sys.argv[0]
        sys.exit(0)


if len(sys.argv) < 3:
        usage()

n = int(sys.argv[1])
sub_n = int(sys.argv[2])

if rank == 0:
	master(n, sub_n)

else:
	slave(n, sub_n)

