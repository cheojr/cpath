##################################################################################
#
# Author: Jose R. Ortiz-Ubarry
# Email:  jose.ortiz@hpcf.upr.edu
# 
# Description: This is a simple implementation to generate Costas array for any
#	       size n.
# 
# Disclaimer: This program was made only for educational purposes under the
#             support of the CPATH project of the University of Puerto Rico.
#             We do not offer any warranties of the software working or used for
#             any other purpose.
#
#            If you use or modify this software please be kind and acknowledge
#            us.
###################################################################################

import sys

def findCostas(costas, n, start, current):
	"""Completes the searching of Costas arrays that start with the subcostas
           received in the variable costas.
	   Pre: Receives a sub array with size less than n, the size of the final array n
                The position where the search space start and the current position to start
                the search.
           Pos: A list with Costas arrays of size n that start with sub array costas."""

	costas_list = []

	while current >= start:
		costas[current] += 1
                while costas[current] in costas[:current] and costas[current] <= n:
               		costas[current]+=1
	
		if costas[current] > n:
                        costas[current] = 0
                        current-=1

		elif isCostas(costas[:current+1], n):
                        if current < n-1:
                                current+=1
                        else:
                                costas_list.append(costas[:])

	return costas_list


def isCostas(array, n):
	"""Checks whether the array received meets the costas property."""

	a_size = len(array)
        for c in range(1, a_size):
        	hash = [0] * (2 * n)
                for i in range(a_size - c):
 	               dif = array[i+c] - array[i]
                       if hash[dif] == 1:
        	               return 0
                       else:
                	       hash[dif] = 1
	return 1

def usage():
        print "Usage: python %s <n Costas size>" % sys.argv[0]
        sys.exit(0)


if len(sys.argv) < 2:
        usage()

n = int(sys.argv[1])
start = 0
current = 1
array = [0] * n

array[0] = 1

for costas in  findCostas(array, n, start, current):
	print costas	
