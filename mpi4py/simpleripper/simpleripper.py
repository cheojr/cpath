##################################################################################
#
# Author: Jose R. Ortiz-Ubarry
# Email:  jose.ortiz@hpcf.upr.edu
# 
# Description: This is a simple implementation to crack crypt shadow files.
# 
# Disclaimer: This program was made only for educational purposes under the
#             support of the CPATH project of the University of Puerto Rico.
#	      We do not offer any warranties of the software working or used for
#	      any other purpose.
#
#   	     If you use or modify this software please be kind and acknowledge
# 	     us.
###################################################################################


import crypt
import sys

def readShadow(path):
	"""Reads a shadow file given its path"""

	return open(path, "r").readlines()	

def readWords(path):
	"""Reads a word dictionary  given its path"""

	return open(path, "r").readlines()

def usage():
	print "Usage: python %s <shadow-file> <words-file>" % sys.argv[0]
	sys.exit(0) 

if len(sys.argv) < 3:
	usage()

# Read shadow and words dictionary
shadow_lines = readShadow(sys.argv[1])
word_lines = readWords(sys.argv[2])

# For each word in the dictionary check againsts password file.

for i in range(len(word_lines)):
	word = word_lines[i].strip()
	for user in shadow_lines:
		user = user.split(":")
		if crypt.crypt(word, user[1]) == user[1]:
			print "User %s, password %s" % (user[0], word)
				
