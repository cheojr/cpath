##################################################################################
#
# Author: Jose R. Ortiz-Ubarry
# Email:  jose.ortiz@hpcf.upr.edu
# 
# Description: This is a simple MPI implementation to crack crypt shadow files.
# 
# Disclaimer: This program was made only for educational purposes under the
#             support of the CPATH project of the University of Puerto Rico.
#             We do not offer any warranties of the software working or used for
#             any other purpose.
#
#            If you use or modify this software please be kind and acknowledge
#            us.
###################################################################################


import crypt
import sys
from mpi4py import MPI 

def readShadow(path):
        """Reads a shadow file given its path"""
	return open(path, "r").readlines()	

def readWords(path):
        """Reads a word dictionary  given its path"""
	return open(path, "r").readlines()

def computeIndices(size, nprocs, rank):
	"""Given a problem size, the number of processes and the rank of the processes
           compute the portion of the problem for the rank"""

        portion = size  / nprocs
        residue = size % nprocs
        
        if rank <= residue:
                n1 = rank * (portion+1)
        else:
                n1 = rank * portion + residue

        n2 = n1 + portion
        if rank < residue:
                n2 += 1

        return n1, n2

def usage():
        print "Usage: python %s <shadow-file> <words-file>" % sys.argv[0]
        sys.exit(0)

#### main ###

comm = MPI.COMM_WORLD   # Defines the default communicator
num_procs = comm.Get_size()  # Stores the number of processes in size.
rank = comm.Get_rank()  # Stores the rank (pid) of the current process
stat = MPI.Status()


if len(sys.argv) < 3:
        usage()

word_lines = readWords(sys.argv[2])

if rank == 0:
	shadow_lines = readShadow(sys.argv[1])
else:
	shadow_lines = None

shadow_lines = comm.bcast(shadow_lines, root=0)

n1, n2 = computeIndices(len(word_lines), num_procs, rank)

# Shrink words from memory.  Trust in the GC
word_lines = word_lines[n1:n2]

for word  in word_lines:
	word = word.strip()
	for user in shadow_lines:
		user = user.split(":")
		user[1] = user[1].strip()
		if crypt.crypt(word, user[1]) == user[1]:
			print "User %s, password %s" % (user[0], word)
				
