##################################################################################
#
# Author: Jose R. Ortiz-Ubarry
# Email:  jose.ortiz@hpcf.upr.edu
# 
# Description: This is a simple implementation to generate Latin squares for any
#              size ni^2.
# 
# Disclaimer: This program was made only for educational purposes under the
#             support of the CPATH project of the University of Puerto Rico.
#             We do not offer any warranties of the software working or used for
#             any other purpose.
#
#            If you use or modify this software please be kind and acknowledge
#            us.
###################################################################################

from mpi4py import MPI
import sys
import itertools

def checkLatin(ls, current, n):
	"""Checks if the new element added to a latin square is not repeated in
          its corresponding column"""

	row = current/n
	column = current % n

	for i in range(row):
		if ls[i * n + column] == ls[current]:
			return False
	
	return True

def completeLatinSquare(latin_square, current, start):
	"""Back tracking algorithm to complete the latin squares that
	   start with the given first row permutation."""

	ls_list = []

	while current > start:
		#find next
		row_s = current/n
		latin_square[current] += 1
		while latin_square[current] in latin_square[row_s*n:current] and latin_square[current] < n:
			latin_square[current]+=1
		
		if latin_square[current] > n:
			#back track
			latin_square[current] = 0
			current-=1
		elif checkLatin(latin_square, current,n):
			if current < size-1:
				current+=1
			else:
				ls_list.append(latin_square[:])

	return ls_list

def usage():
        print "Usage: python %s <n size of the rows and column of a Latin square>" % sys.argv[0]
        sys.exit(0)


if len(sys.argv) < 2:
        usage()

n = int(sys.argv[1])
size = pow(n,2)

# We start adding values to the latin square in the second row of the square.  Thus in position n.
current = n
start = n-1

ls_list = []

# Get all the permutations of size n to use as the first row of the LS we will search.
perm_list = []
perm_list = [perm for perm in itertools.permutations([i+1 for i in range(n)])]

# Search all the LS
fd = open("serial_ls_%s.txt" % n, "w")	
for i in range(len(perm_list)):
	latin_square = list(perm_list[i]) + [0] * (size - n)
	ls_list += completeLatinSquare(latin_square, current, start)

for ls in ls_list:
	fd.write("%s\n" % ls)
