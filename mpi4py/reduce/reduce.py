from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

val = 3
sum = comm.reduce(val, op=MPI.SUM, root =0)

if rank == 0:
        print "The reduction is %s" % sum
